<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: x_mar
  Date: 09/12/2022
  Time: 10:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Détail du patient</title>
    <jsp:include page="/WEB-INF/includes/head.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp"></jsp:include>
<main>
    <ul>
        <li>${patient.getPrenom()} ${patient.getNom()}</li>
        <li>${patient.getNumeroSecu()}</li>
        <li>${patient.getDateNaissance()} ${patient.getSexe()}</li>
        <li>${patient.getAdresse()}</li>
        <li>${patient.getTel()}</li>
    </ul>

    <div>
        <p>Dossier médical créé le ${dossier.getDateCreation()}</p>
    </div>

    <ul>
        <c:forEach items="${consultations}" var="consultation">
            <li>${consultation.getDateConsultation()}</li>
            <li>${consultation.getLieu()}</li>
            <li>Afficher les détails de cette consultation <a href="${pageContext.request.contextPath}/consultation?idConsult=${consultation.getNumeroConsultation()}">🔎</a></li>
        </c:forEach>
    </ul>

    <a>Ajouter une consultation</a>
</main>
</body>
</html>
