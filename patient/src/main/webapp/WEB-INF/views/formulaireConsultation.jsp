<%--
  Created by IntelliJ IDEA.
  User: x_mar
  Date: 09/12/2022
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ajouter une consultation</title>
    <jsp:include page="/WEB-INF/includes/head.jsp"></jsp:include>
</head>
<body>
<main>
<%--  TESTER 2 actions => 1 pour juste ajouter une consultation et pouvoir la supprimer ensuite--%>
<%--  1 pour ajouter directement prescription + analyses--%>
  <form method="post" action="">
    <div>
      <input type="date" name="date" />
    </div>
    <div>
      <input type="text" name="heures" placeholder="HH:MM" />
    </div>
    <div>
      <input type="text" name="lieu" placeholder="Lieu de la consultation" />
    </div>
    <button>Ajouter la consultation</button>
    <button formaction="">Ajouter une prescription</button>
  </form>
</main>
</body>
</html>
