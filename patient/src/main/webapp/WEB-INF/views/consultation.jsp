<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: x_mar
  Date: 09/12/2022
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Détails de la consultation</title>
    <jsp:include page="/WEB-INF/includes/head.jsp"></jsp:include>
</head>
<body>
<main>
    <jsp:include page="/WEB-INF/includes/header.jsp"></jsp:include>
    <h2>Consultation du ${consultation.getDateConsultation()}</h2>

    <ul>
      <c:forEach items="${prescriptions}" var="prescription">
          <li>${prescription.getDesignation()}</li>
          <li>${prescription.getPeriode()}</li>
          <li>${prescription.getIndication()}</li>
      </c:forEach>
    </ul>

    <ul>
        <c:forEach items="${analyses}" var="analyse">
            <li>${analyse.getDescription()}</li>
            <li>${analyse.getResultat()}</li>
            <li>${analyse.getDateHeure()}</li>
        </c:forEach>
    </ul>

    <p>${fiche.getCompteRendu()}</p>
</main>
</body>
</html>
