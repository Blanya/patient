<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: x_mar
  Date: 08/12/2022
  Time: 14:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ajouter un patient</title>
    <jsp:include page="/WEB-INF/includes/head.jsp"></jsp:include>
</head>
<body>
<%--<header>--%>
<%--  <h1>Ajouter un patient</h1>--%>
<%--</header>--%>
<jsp:include page="/WEB-INF/includes/header.jsp"></jsp:include>
<main>

<%--  Message informatif changeant => patient puis dossier--%>
    <p class="text-center">${info}</p>

<%--  Si patient ajouté après soumission du form, création du dossier--%>
  <c:if test="${dossier}">
  <form method="post" action="ajouterPatient">
    <div class="row justify-content-center m-1">
      <label for="id">Numéro du patient</label>
      <input type="text" value="${id}" name="id" id="id" readonly />
    </div>
    <div class="row justify-content-center m-1">
      <label>Code d'accès au dossier médical</label>
      <input type="text" name="code"/>
    </div>

    <button type="submit">Enregistrer</button>
  </form>
  </c:if>

  <c:if test="${ajout}">
    <form method="post" action="ajouterPatient" class="container">
      <div class="row justify-content-center m-1">
        <input type="text" class="form-control p-1" name="nom" placeholder="Nom"/>
      </div>
      <div class="row justify-content-center m-1">
        <input type="text" class="form-control p-1" name="prenom" placeholder="Prenom"/>
      </div>
      <div class="justify-content-center m-1">
        <label>Genre: </label>
        <label for="f">Féminin</label>
        <input type="radio" name="genre" value="F" id="f">
        <label for="m">Masculin</label>
        <input type="radio" name="genre" value="M" id="m">
      </div>
      <div class="row justify-content-center m-1">
        <input type="text" class="form-control p-1" name="ss" placeholder="Numéro de sécurité sociale"/>
      </div>
      <div class="row justify-content-center m-1">
        <input type="text" class="form-control p-1" name="adresse" placeholder="Adresse complète"/>
      </div>
      <div class="row justify-content-center m-1">
        <input type="text" class="form-control p-1" name="tel" placeholder="Numéro de téléphone"/>
      </div>
      <div class="row justify-content-center m-1">
        <label for="naissance" class="p-0">Date de naissance</label>
        <input type="date" class="form-control p-1" name="naissance" id="naissance" />
      </div>

      <button class="btn btn-outline-success m-1" type="submit">Valider</button>
    </form>
  </c:if>


</main>
</body>
</html>
