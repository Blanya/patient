<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Gestion de Patients</title>
    <jsp:include page="WEB-INF/includes/head.jsp"></jsp:include>
</head>
<body>
    <jsp:include page="WEB-INF/includes/header.jsp"></jsp:include>
<main>
    <form method="get" action="${pageContext.request.contextPath}/">
        <div>
            <input type="search" name="patientName" placeholder="Nom du patient" />
            <button>Rechercher</button>
        </div>
    </form>


<%--    Aucun patient trouvé--%>
        <p>${message}</p>

    <c:if test="${patients.size()>0}">
        <ul>
            <c:forEach items="${patients}" var="patient">
                <li>${patient.getNom()} ${patient.getPrenom()} <a href="?id=${patient.getId()}">🔎</a></li>
            </c:forEach>
        </ul>
    </c:if>

<%--&lt;%&ndash;    Lien vers la création d'un patient &ndash;%&gt;--%>
<%--    <a href="ajouterPatient">Ajouter un patient</a>--%>
</main>
</body>
</html>