package com.example.patient.repositories.impl;

import com.example.patient.repositories.IDao;
import com.example.patient.utils.HibernateSession;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;

public abstract class BaseRepository<T> implements IDao<T> {
    protected Transaction transaction;
    protected BaseRepository(){
        transaction = HibernateSession.getInstance().getTransaction();
    }


    @Override
    public boolean create(T o) {
        Serializable s = HibernateSession.getInstance().save(o);
        transaction.commit();
        HibernateSession.getInstance().close();
        return s != null;
    }

    @Override
    public boolean update(T o) {
        return false;
    }

    @Override
    public boolean delete(T o) {
        return false;
    }

    @Override
    public T findById(int id) {
        return null;
    }

    @Override
    public List<T> findAll() {
        return null;
    }
}

