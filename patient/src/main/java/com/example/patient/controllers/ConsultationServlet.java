package com.example.patient.controllers;

import com.example.patient.models.Consultation;
import com.example.patient.models.FicheConsultation;
import com.example.patient.models.Patient;
import com.example.patient.services.ConsultationService;
import com.example.patient.services.FicheConsultationService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "ConsultationServlet", value = "/consultation")
public class ConsultationServlet extends HttpServlet {

    private ConsultationService consultationService;
    private FicheConsultationService ficheConsultationService;

    @Override
    public void init() throws ServletException {
        consultationService = new ConsultationService();
        ficheConsultationService = new FicheConsultationService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("idConsult") != null) {
            int id = Integer.parseInt(req.getParameter("idConsult"));
            Consultation consultation = consultationService.findById(id);
            if(consultation != null) {
                FicheConsultation fiche = consultation.getFicheConsultation();
                req.setAttribute("consultation", consultation);
                req.setAttribute("fiche", fiche);
                req.setAttribute("analyses", fiche.getAnalyses());
                req.setAttribute("prescriptions", fiche.getPrescriptions());
                req.getRequestDispatcher("/WEB-INF/views/consultation.jsp").forward(req, resp);
            }else {
                req.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(req,resp);
            }
        }
    }
}
