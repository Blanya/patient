package com.example.patient.controllers;

import com.example.patient.models.DossierMedical;
import com.example.patient.models.Patient;
import com.example.patient.services.DossierService;
import com.example.patient.services.PatientService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "CreatePatientServlet", value = "/ajouterPatient")
public class CreatePatientServlet extends HttpServlet {

    private PatientService patientService;
    private DossierService dossierService;

    public void init(){
        patientService = new PatientService();
        dossierService = new DossierService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("ajout", true );
        req.getRequestDispatcher("WEB-INF/views/createPatient.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(req.getParameter("nom") == null)
        {
            if(!req.getParameter("code").isBlank()) {
                Date date = new Date();
                String code = req.getParameter("code");

                if (req.getParameter("id") != null) {
                    DossierMedical dossierMedical = new DossierMedical(date, code);

                    int id = Integer.parseInt((String) req.getParameter("id"));
                    Patient patient = patientService.findById(id);

                    if (patient != null) {
                        boolean createDossier = dossierService.create(dossierMedical);
                        dossierMedical.setPatient(patient);
                        patientService.update(patient);

                        if (createDossier) {
                            req.setAttribute("info", "Le dossier médical a bien été créé");
                            req.setAttribute("ajout", true);
                            req.getRequestDispatcher("WEB-INF/views/createPatient.jsp").forward(req, resp);
                        } else {
                            req.getRequestDispatcher("WEB-INF/views/error.jsp").forward(req, resp);
                        }
                    } else {
                        req.getRequestDispatcher("WEB-INF/views/error.jsp").forward(req, resp);
                    }
                } else {
                    req.getRequestDispatcher("WEB-INF/views/error.jsp").forward(req, resp);
                }
            }}
        //Verif des params non vides =>
//        else if(!req.getParameter("nom").isBlank() && !req.getParameter("prenom").isBlank() && !req.getParameter("genre").isBlank() && !req.getParameter("adresse").isBlank() && !req.getParameter("tel").isBlank() && !req.getParameter("ss").isBlank() && !req.getParameter("naissance").isBlank())
//        {
//            String prenom = req.getParameter("prenom");
//            String nom = req.getParameter("nom");
//            char genre  = req.getParameter("genre").charAt(0);
//            String ss = req.getParameter("ss");
//            String adresse = req.getParameter("adresse");
//            String tel = req.getParameter("tel");
//
//
//            Date date;
//            try {
//                date = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("naissance"));
//            } catch (ParseException e) {
//                //ERROR
//                date = null;
//            }
//
//            Patient p = new Patient(ss, prenom, nom, date, genre, adresse, tel);
//
//
//            if(patientService.create(p))
//            {
//                boolean message = patientService.create(p);
//                req.setAttribute("info", "Le patient " + nom + " " + prenom + " a bien été créé");
//                req.setAttribute("id", p.getId());
//                req.setAttribute("dossier", true);
//                req.getRequestDispatcher("WEB-INF/views/createPatient.jsp").forward(req, resp);
//            }
//            else {
//                req.getRequestDispatcher("WEB-INF/views/error.jsp").forward(req, resp);
//            }
//        }
//        else {
//            req.getRequestDispatcher("WEB-INF/views/error.jsp").forward(req, resp);
//        }


        //Verification des champs par le service =>

        String prenom = req.getParameter("prenom");
        String nom = req.getParameter("nom");
        char genre  = req.getParameter("genre").charAt(0);
        String ss = req.getParameter("ss");
        String adresse = req.getParameter("adresse");
        String tel = req.getParameter("tel");
        String date = req.getParameter("naissance");

        try {
//            patientService.create(nom, prenom, tel, adresse, ss, date, genre);
//            req.setAttribute("info", "Le patient " + nom + " " + prenom + " a bien été créé");
//            req.setAttribute("id", p.getId());
//            req.setAttribute("dossier", true);
//            req.getRequestDispatcher("WEB-INF/views/createPatient.jsp").forward(req, resp);
        }catch (Exception e)
        {
            //ERROR
        }
    }

    public void destroy(){

    }
}
