package com.example.patient.controllers;

import com.example.patient.models.Patient;
import com.example.patient.services.ConsultationService;
import com.example.patient.services.DossierService;
import com.example.patient.services.PatientService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "PatientServlet", value = "")
public class PatientsServlet extends HttpServlet {
    private PatientService patientService;
    private DossierService dossierService;
    private ConsultationService consultationService;

    @Override
    public void init() throws ServletException {
        patientService = new PatientService();
        consultationService = new ConsultationService();
        dossierService = new DossierService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id") != null) {
            int id = Integer.parseInt(req.getParameter("id"));
            Patient patient = patientService.findById(id);
            if(patient != null) {
                req.setAttribute("patient", patient);
                req.setAttribute("dossier", dossierService.findByPatient(patient));
                req.setAttribute("consultations", consultationService.findAllByPatient(patient));
                req.getRequestDispatcher("/WEB-INF/views/patient.jsp").forward(req, resp);
            }else {
                req.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(req,resp);
            }
        }
        else if(req.getParameter("patientName") != null){
            if(!req.getParameter("patientName").isBlank()){
                String name = req.getParameter("patientName");
                List<Patient> patients = null;
                try{
                    patients = patientService.findByName(name);
                }catch (Exception e){
                    //PAGE ERREUR
                }

                if(patients.size()>0){
                    req.setAttribute("patients", patients);
                    req.getRequestDispatcher("index.jsp").forward(req, resp);
                } else {
                    req.setAttribute("message","Aucun patient ne correspond à votre recherche, veuillez réessayer");
                    req.getRequestDispatcher("index.jsp").forward(req, resp);
                }
            }else {
                req.setAttribute("patients", patientService.findAll());
                req.getRequestDispatcher("index.jsp").forward(req, resp);
            }
        }else{
            req.setAttribute("patients", patientService.findAll());
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        }
    }
}
