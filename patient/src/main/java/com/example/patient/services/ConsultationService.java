package com.example.patient.services;


import com.example.patient.repositories.IDao;
import com.example.patient.models.*;

import java.util.ArrayList;
import java.util.List;

public class ConsultationService extends BaseService implements IDao<Consultation> {

    @Override
    public boolean create(Consultation o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(Consultation o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(Consultation o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public Consultation findById(int id) {
        Consultation consultation = session.get(Consultation.class, id);
        return consultation;
    }

    @Override
    public List<Consultation> findAll() {
        List<Consultation> consultations = null;
        consultations = session.createQuery("FROM Consultation").list();
        return consultations;
    }

    //Consultation d'un patient=> dossier du patient => fiche de consultation => origin consultation
    public List<Consultation> findAllByPatient(Patient patient)
    {
        List<Consultation> consultations = new ArrayList<>();
        DossierMedical dossierMedical = new DossierService().findByPatient(patient);
        List<FicheDeSoin> fiches = dossierMedical.getFiches();
        if(fiches.size()>0){
            for (FicheDeSoin f: fiches) {
                if(f instanceof FicheConsultation){
                    consultations.add(((FicheConsultation) f).getOrigineFiche());
                }
            }
            return consultations;
        }else
            return null;
    }
}
