package com.example.patient.services;

import com.example.patient.repositories.IDao;
import com.example.patient.models.DossierMedical;
import com.example.patient.models.Patient;

import java.util.List;

public class DossierService extends BaseService implements IDao<DossierMedical> {
    @Override
    public boolean create(DossierMedical o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(DossierMedical o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(DossierMedical o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public DossierMedical findById(int id) {
        //Si non toruvé return null
        DossierMedical dossierMedical = session.get(DossierMedical.class, id);
        return dossierMedical;
    }

    @Override
    public List<DossierMedical> findAll() {
        List<DossierMedical> dossiers = null;
        dossiers = session.createQuery("FROM DossierMedical").list();
        return dossiers;
    }

    //Retourne le dossier médical d'un patient
    public DossierMedical findByPatient(Patient patient)
    {
        DossierMedical dossierMedical = null;
        dossierMedical = patient.getDossierMedical();
        return dossierMedical;
    }
}

