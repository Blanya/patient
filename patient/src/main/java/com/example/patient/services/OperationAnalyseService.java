package com.example.patient.services;

import com.example.patient.repositories.IDao;
import com.example.patient.models.OperationAnalyse;

import java.util.List;

public class OperationAnalyseService extends BaseService implements IDao<OperationAnalyse> {
    @Override
    public boolean create(OperationAnalyse o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(OperationAnalyse o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(OperationAnalyse o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public OperationAnalyse findById(int id) {
        OperationAnalyse operationAnalyse = session.get(OperationAnalyse.class, id);
        return operationAnalyse;
    }

    @Override
    public List<OperationAnalyse> findAll() {
        List<OperationAnalyse> operationAnalyses = null;
        operationAnalyses = session.createQuery("FROM OperationAnalyse").list();
        return operationAnalyses;
    }
}
