package com.example.patient.services;

import com.example.patient.repositories.IDao;
import com.example.patient.models.Patient;
import com.example.patient.repositories.impl.BaseRepository;

import java.util.List;

public class PatientService extends BaseService implements IDao<Patient>{

//    private BaseRepository<Patient> _patientRepository;
//
//    public PatientService(BaseRepository<Patient> patientBaseRepository){
//        _patientRepository = patientBaseRepository;
//    }
//
//    public boolean create(String nom, String prenom, String telephone, String adresse, String nss, String dateNaissance, String sexe) throws StringFormatException, ParseException{
//        //logique métier => exception ds les Setter directement
//        Patient patient = new Patient();
//        patient.setNom(nom);
//        patient.setPrenom(prenom);
//        patient.setNumeroSecu(nss);
//        patient.setAdresse(adresse);
//        patient.setSexe(sexe.charAt(0));
//        patient.setTel(telephone);
//        Date dateN = new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance);
//    patient.setDateNaissance(dateN);
//        return _patientRepository.create(patient);
//    }
    @Override
    public boolean create(Patient o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(Patient o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(Patient o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public Patient findById(int id) {
        //Si non toruvé return null
        Patient patient = session.get(Patient.class, id);
        return patient;
    }

    @Override
    public List<Patient> findAll() {
        List<Patient> patients = null;
        patients = session.createQuery("FROM Patient").list();
        return patients;
    }

    //Trouver patient par nom
    public List<Patient> findByName(String name){
        List<Patient> patients = null;
        patients = session.createQuery("FROM Patient WHERE nom LIKE :name").setParameter("name", name+'%').list();
        return patients;
    }
}

