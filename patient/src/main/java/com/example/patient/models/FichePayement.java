package com.example.patient.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "fiche_payement")
public class FichePayement extends FicheDeSoin{
    @Column(name = "date_ex")
    private Date dateExigibiliteP;

    @Column(name = "date_pay")
    private Date datePayement;

    @Column(name = "montant_pay")
    private double montantPaye;

    @Column(name = "montant_don")
    private double montantDonne;

    @Column(name = "payement_eff")
    private boolean indicateurPayement;

    public FichePayement() {
    }


    public FichePayement(Date dateCreation, String agentCreateur, String adresseCreateur, Date dateExigibiliteP, Date datePayement, double montantPaye, double montantDonne, boolean indicateurPayement) {
        super(dateCreation, agentCreateur, adresseCreateur);
        this.dateExigibiliteP = dateExigibiliteP;
        this.datePayement = datePayement;
        this.montantPaye = montantPaye;
        this.montantDonne = montantDonne;
        this.indicateurPayement = indicateurPayement;
    }

    public FichePayement(Date dateCreation, String agentCreateur, String adresseCreateur, Date dateExigibiliteP, double montantPaye) {
        super(dateCreation, agentCreateur, adresseCreateur);
        this.dateExigibiliteP = dateExigibiliteP;
        this.montantPaye = montantPaye;
        //par défaut si montant donné = 0
        this.indicateurPayement = false;
    }



    public Date getDateExigibiliteP() {
        return dateExigibiliteP;
    }

    public void setDateExigibiliteP(Date dateExigibiliteP) {
        this.dateExigibiliteP = dateExigibiliteP;
    }

    public Date getDatePayement() {
        return datePayement;
    }

    public void setDatePayement(Date datePayement) {
        this.datePayement = datePayement;
    }

    public double getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(double montantPaye) {
        this.montantPaye = montantPaye;
    }

    public boolean isIndicateurPayement() {
        return indicateurPayement;
    }

    public void setIndicateurPayement(boolean indicateurPayement) {
        this.indicateurPayement = indicateurPayement;
    }

    //method regulariser les paiements
    public void regulariserPayement()
    {

    }

    @Override
    public String toString() {
        return "FichePayement{" +
                "dateExigibiliteP=" + dateExigibiliteP +
                ", datePayement=" + datePayement +
                ", montantPaye=" + montantPaye +
                ", indicateurPayement=" + indicateurPayement +
                '}';
    }
}


