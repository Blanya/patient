package com.example.patient.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "fiche_consultation")
public class FicheConsultation extends FicheDeSoin{

    @Column(name = "compte_rendu")
    private String compteRendu;

    @OneToMany(cascade = CascadeType.ALL)
    private List<OperationAnalyse> analyses;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Prescription> prescriptions;

    @OneToOne
    @JoinColumn(name = "num_cons")
    private Consultation origineFiche;

    public FicheConsultation(){

    }

    public FicheConsultation(String compteRendu) {
        this.compteRendu = compteRendu;
    }

    public FicheConsultation(Date dateCreation, String agentCreateur, String adresseCreateur, String compteRendu) {
        super(dateCreation, agentCreateur, adresseCreateur);
        this.compteRendu = compteRendu;
    }

    public String getCompteRendu() {
        return compteRendu;
    }

    public void setCompteRendu(String compteRendu) {
        this.compteRendu = compteRendu;
    }

    public List<OperationAnalyse> getAnalyses() {
        return analyses;
    }

    public void setAnalyses(List<OperationAnalyse> analyses) {
        this.analyses = analyses;
    }

    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public Consultation getOrigineFiche() {
        return origineFiche;
    }

    public void setOrigineFiche(Consultation origineFiche) {
        this.origineFiche = origineFiche;
    }
}
