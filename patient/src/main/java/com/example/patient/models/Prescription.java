package com.example.patient.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "prescription")
//USE lombok => Getter et Setter
//@Data
public class Prescription {
    @Id
    @Column(name = "num_pres")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String designation;

    private String periode;

    private String indication;

    public Prescription() {
    }

    public Prescription(String designation, String periode) {
        this.designation = designation;
        this.periode = periode;
    }

    public Prescription(String designation, String periode, String indication) {
        this.designation = designation;
        this.periode = periode;
        this.indication = indication;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    @Override
    public String toString() {
        return "Prescription{" +
                "id=" + id +
                ", designation='" + designation + '\'' +
                ", periode='" + periode + '\'' +
                ", indication='" + indication + '\'' +
                '}';
    }
}

