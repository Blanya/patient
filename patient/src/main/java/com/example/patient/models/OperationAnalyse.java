package com.example.patient.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "operation_analyse")
public class OperationAnalyse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_an")
    private int id;

    private String description;

    @Column(name = "date_heure_op")
    private Date dateHeure;

    private String resultat;

    public OperationAnalyse() {
    }

    public OperationAnalyse(String description, Date dateHeure, String resultat) {
        this.description = description;
        this.dateHeure = dateHeure;
        this.resultat = resultat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateHeure() {
        return dateHeure;
    }

    public void setDateHeure(Date dateHeure) {
        this.dateHeure = dateHeure;
    }

    public String getResultat() {
        return resultat;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    @Override
    public String toString() {
        return "OperationAnalyse{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", dateHeure=" + dateHeure +
                ", resultat='" + resultat + '\'' +
                '}';
    }
}

