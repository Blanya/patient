package com.example.patient.models;

import com.example.patient.exception.StringFormatException;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "patient")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="pa_nss")
    private String numeroSecu;

    private String prenom;

    private String nom;

    @Column(name = "date_de_naissance")
    @Temporal(TemporalType.DATE)
    private Date dateNaissance;

    private char sexe;

    private String adresse;

    private String tel;

    @OneToOne
    @JoinColumn(name = "num_do")
    private DossierMedical dossier;

    public Patient() {
    }

    public Patient(String numeroSecu, String prenom, String nom, Date dateNaissance, char sexe, String adresse, String tel) {
        this.numeroSecu = numeroSecu;
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.sexe = sexe;
        this.adresse = adresse;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroSecu() {
        return numeroSecu;
    }

    public void setNumeroSecu(String numeroSecu) {
        this.numeroSecu = numeroSecu;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) throws StringFormatException{
        if(!prenom.isBlank() && prenom.length() > 2)
            this.prenom = prenom;
        else
            throw new StringFormatException();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) throws StringFormatException {
        if(!nom.isBlank() && nom.length() >= 2)
            this.nom = nom;
        else
            throw new StringFormatException();
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dataNaissance) {
        this.dateNaissance = dataNaissance;
    }

    public char getSexe() {
        return sexe;
    }

    public void setSexe(char sexe) throws StringFormatException{
        if(sexe>0)
            this.sexe = sexe;
        else
            throw new StringFormatException();

    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) throws StringFormatException {
        if(!adresse.isBlank() && adresse.length() > 5)
            this.adresse = adresse;
        else
            throw new StringFormatException();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) throws StringFormatException{
        if(!tel.isBlank() && tel.length() > 9)
            this.tel = tel;
        else
            throw new StringFormatException();
    }

    public DossierMedical getDossierMedical() {
        return dossier;
    }

    public void setDossierMedical(DossierMedical dossierMedical) {
        this.dossier = dossierMedical;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "numeroSecu='" + numeroSecu + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", sexe=" + sexe +
                ", adresse='" + adresse + '\'' +
                ", tel='" + tel + '\'' +
                ", dossierMedical=" + dossier+
                '}';
    }
}
